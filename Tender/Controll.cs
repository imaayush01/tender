﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Tender
{
    public class Controll
    {
        public static DataSet ds;
        public static SqlConnection conn = null;
        public static SqlConnection connected()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
            conn.Open();
            return conn;
        }
        public static DataSet SelectId(string query)
        {
            SqlCommand cmd = new SqlCommand(query, connected());
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            ds = new DataSet(); da.Fill(ds);
            return ds;
        }
    }
}